var init = 0 ;
var clock = new
  struct
    var time = init;
    proc tick() { time = (time + 1) };
    proc display() { print time };
    proc reset() { time = 0 }
  end;

clock.tick();
clock.tick();
clock.display()