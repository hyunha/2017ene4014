import scala.io.Source
import scala.util.parsing.combinator.JavaTokenParsers

trait ExprAST {
  sealed abstract class Etree
  case class Add(e1: Etree, e2: Etree) extends Etree
  case class Sub(e1: Etree, e2: Etree) extends Etree
  case class Mul(e1: Etree, e2: Etree) extends Etree
  case class Div(e1: Etree, e2: Etree) extends Etree
  case class Num(n: String) extends Etree
}

object Expression extends JavaTokenParsers with ExprAST {
  // Parser
  def parse(source: String): Etree =
    parseAll(expr, source) match {
      case Success(optree,_) => optree
      case _ => throw new Exception("Parse error!")
    }
  def expr: Parser[Etree] = chainl1(term, "+" ^^^ Add | "-" ^^^ Sub)
  def term: Parser[Etree] = chainl1(factor, "*" ^^^ Mul | "/" ^^^ Div)
  def factor: Parser[Etree] =
    floatingPointNumber ^^ Num |
      "("~>expr<~")"

  // Controller
  def main(args: Array[String]): Unit = {
    try {
      val source =
        Source.fromFile("src/main/resources/expr.txt").getLines.mkString
      println("input : " + source)
      val optree = parse(source)
      println("optree : " + optree)
    } catch {
      case e: Exception => println(e)
    }
  }
}
