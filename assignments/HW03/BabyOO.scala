// Interpreter for a Baby object-oriented language
//   with Declarations,
//        Type Templates,
//        Procedures,
//        Classes, and Modules.
//
// P : Program                 E : Expression
// D : Declaration             L : NameExpression
// T : TypeTemplate            N : Numeral
// C : Command                 I : Identifier
//
// P  ::= D ; C
// E  ::= N | L | ( E1 + E2 ) | ( E1 == E2 ) | not E | new T | nil
// C  ::= L = E | print L | C1 ; C2
//      | if E { C1 } else { C2 } | while E { C } | L()
// D  ::= var I = E | D1 ; D2 | proc I(I+) { C } | class I = T
//      | module I = D end | import L
// T  ::= struct D end | array [ E1 ] of E2 | L
// L  ::= I | L . I | L [ E ]
// N  ::= string of digits
// I  ::= strings of letters, not including keywords
//
// Operator Tree
// PTREE ::= Program(DTREE, CTREE)
// ETREE ::= Num(NUM) | Deref(LTREE) | Add(ETREE, ETREE)
//         | Eq(ETREE, ETREE) | Neq(ETREE) | New(TREE) | NIL
// CTREE ::= Assign(LTREE, ETREE) | Print(LTREE) | Seq(CTREE, CTREE)
//         | Cond(ETREE, CTREE, CTREE) | Loop(ETREE, CTREE) | Proc(ID, ETREE*)
// DTREE ::= DVar(ID, ETREE) | DSeq(DTREE, DTREE) | DProc(ID, ID*, C)
//         | Class(ID, TTREE) | Module(ID, DTREE) | Import(LTREE)
// TTREE ::= TStruct(DTREE) | TArray(ETREE, ETREE) | TName(LTREE)
// LTREE ::= Id(ID) | Dot(LTREE,LTREE) | Arr(LTREE, ETREE)
// NUM   ::= a nonempty string of digits
// ID    ::= a nonempty string of letters

trait OpTree {
  sealed abstract class Rval {
    // r1.+(r2)
    // r1 + r2
    def +(r:Rval): Rval = {
      this match {
        case IntValue(n1) =>
          r match {
            case IntValue(n2) => IntValue(n1 + n2)
            case _ => throw new Exception("illegal operand of +")
          }
        case _ => throw new Exception("illegal operand of +")
      }
    }

    def to_greek: String = {
      this match {
        case IntValue(n) => n.toString
        case BoolValue(b) => b.toString
        case Handle(h) =>
          h match {
            case 0 => "α"
            case 1 => "β"
            case 2 => "γ"
            case 3 => "δ"
            case 4 => "ε"
            case 5 => "ζ"
            case 6 => "η"
            case 7 => "θ"
            case 8 => "ι"
            case 9 => "κ"
            case 10 => "λ"
            case 11 => "μ"
            case 12 => "ν"
            case 13 => "ξ"
            case 14 => "ο"
            case 15 => "π"
            case 16 => "ρ"
            case 17 => "σ"
            case 18 => "τ"
            case 19 => "υ"
            case 20 => "φ"
            case 21 => "χ"
            case 22 => "ψ"
            case 23 => "ω"
            case n => "`" + n.toString
          }
        case NILValue => "nil"
      }
    }
  }
  case class Handle(loc: Int) extends Rval
  case class IntValue(n: Int) extends Rval
  case class BoolValue(b: Boolean) extends Rval
  case object NILValue extends Rval

  sealed abstract class Ptree
  case class Program(d: Dtree, c: Ctree) extends Ptree

  sealed abstract class Ltree
  case class Id(x: String) extends Ltree
  case class Dot(ns: Ltree, x:Ltree) extends Ltree
  case class Arr(an: Ltree, i:Etree) extends Ltree

  sealed abstract class Ttree
  case class TStruct(d: Dtree) extends Ttree
  case class TArray(len: Etree, init: Etree) extends Ttree
//  case class TName(t: Ltree) extends Ttree

  sealed abstract class Dtree
  case class DVar(id: String, init: Etree) extends Dtree
  case class DSeq(d1: Dtree, d2: Dtree) extends Dtree
  case class DProc(f: String, fp:List[String], body: Ctree) extends Dtree
//  case class DClass(cn: String, t:Ttree) extends Dtree
//  case class DModule(m: String, d:Dtree) extends Dtree
//  case class DImport(m: Ltree) extends Dtree

  sealed abstract class Etree
  case class Num(s: String) extends Etree
  case class Deref(l: Ltree) extends Etree
  case class Add(e1: Etree, e2: Etree) extends Etree
  case class Eq(e1: Etree, e2: Etree) extends Etree
  case class Neg(e: Etree) extends Etree
  case class New(t: Ttree) extends Etree
  case object NIL extends Etree

  sealed abstract class Ctree
  case class Assign(l: Ltree, e: Etree) extends Ctree
  case class Cond(e: Etree, ct: Ctree, cf: Ctree) extends Ctree
  case class Loop(e: Etree, c:Ctree) extends Ctree
  case class Print(e: Etree) extends Ctree
  case class Seq(c1: Ctree, c2: Ctree) extends Ctree
  case class Proc(f: Ltree, es: List[Etree]) extends Ctree
}

import scala.io.Source
import scala.util.parsing.combinator.JavaTokenParsers

object BabyOO extends JavaTokenParsers with OpTree {

  // Parser
  def parse(source: String): Ptree =
    parseAll(prog, source) match {
      case Success(optree,_) => optree
      case Error(msg, _) => throw new Exception("Parse error! : " + msg)
      case Failure(msg, _) => throw new Exception("Parse Failure! : " + msg)
    }

  // Program P ::= D ;; C
  def prog: Parser[Ptree] =
    decls~";"~comms ^^ { case d~_~c => Program(d,c) }

  def decls: Parser[Dtree] =
    rep1sep(decl, ";") ^^ {
      case h :: t =>
        t.foldLeft(h:Dtree)((pre, d) => DSeq(pre, d))
      case _ => throw new Exception("[parsing][decls] Will never be selected.")
    }
  def decl: Parser[Dtree] =
    ("var"~>ident<~"=")~expr ^^ { case id~e => DVar(id, e) } |
    ("proc"~>ident~("("~>repsep(ident,",")<~")"))~("{"~>comms<~"}") ^^
      { case f~ps~c => DProc(f, ps, c) }

  // Command C ::= L = E | print L | if E : CL1 else CL2 end
  def comms: Parser[Ctree] =
    rep1sep(comm, ";") ^^ {
      case h :: t =>
        t.foldLeft(h:Ctree)((pre, c) => Seq(pre, c))
      case _ => throw new Exception("[parsing][comms] Will never be selected.")
    }

  def comm: Parser[Ctree] =
    (left<~"=")~expr ^^ { case l~e => Assign(l,e) } |
    "print"~>expr ^^ Print |
    ("if"~>expr<~":")~(comms<~"else")~(comms<~"end") ^^
      { case e~ct~cf => Cond(e,ct,cf) } |
    ("while"~>expr)~("{"~>comms<~"}") ^^ { case e~c => Loop(e, c) } |
    left~("("~>repsep(expr, ",")<~")") ^^ { case f~es => Proc(f, es) }

  // Expression E ::= N | L | ( E1 + E2 ) | ( E1 == E2 ) | new T
  def expr: Parser[Etree] =
    "(" ~> expr~("+"~>expr) <~ ")" ^^ { case e1~e2 => Add(e1,e2) } |
    "(" ~> expr~("=="~>expr) <~ ")" ^^ { case e1~e2 => Eq(e1,e2) } |
    ("not"~>expr) ^^ Neg |
    wholeNumber ^^ Num |
    "new"~>tmpl ^^ New |
    left ^^ Deref |
    "nil" ^^ { case _ => NIL }

  def tmpl: Parser[Ttree] =
    "struct"~>decls<~"end" ^^ TStruct |
    "array"~>("["~>expr<~"]")~("of"~>expr) ^^ { case e1~e2 => TArray(e1,e2) }

  def left: Parser[Ltree] =
    rep1sep(ident, ".")~opt("["~>expr<~"]") ^^ {
      case (h :: t) ~ e_opt =>
        val id = t.foldLeft(Id(h):Ltree)((l, id) => Dot(l, Id(id)))
        e_opt match {
          case None => id
          case Some (e) => Arr(id, e)
        }
      case Nil~_ =>
        throw new Exception("unexpected case")
    }

  var heap_to_greek = false

  // Interpreter
  var heap : Map[Handle, Map[String,Rval]] = Map()
  var heap_counter : Int = 0
  def newHandle(): Handle = {
    val newhandle = heap_counter
    heap_counter = heap_counter + 1
    Handle(newhandle)
  }

  def print_heap(): Unit = {
    for ((h, m) <- heap) {
      if (heap_to_greek) println(h.to_greek + " -> [")
      else println(h + " -> [")
        for ((x, rval) <- m)
          if (heap_to_greek) println("  " + x + " -> " + rval.to_greek)
          else println("  " + x + " -> " + rval)
      println("]")
    }
  }

  var closure : Map[Handle, (List[String], Ctree, Handle)] = Map()

  def print_closure(): Unit = {
    println("closure : [")
    for ((h, triple) <- closure) {
      if (heap_to_greek)
        println("  " + h.to_greek + " -> " + triple._1 + " : " + triple._3.to_greek)
      else
        println("  " + h + " -> " + triple._1 + " : " + triple._3)
    }
    println("]")
  }

  val parent_ns_key = "$parentns$"

  def allocateNS(parent: Rval = NILValue): Handle = {
    val newhandle = newHandle()
    heap += (newhandle -> Map(parent_ns_key -> parent))
    newhandle
  }

  var ns: List[Handle] = List(allocateNS())

  def push_ns(h: Handle): Unit =
    ns = h :: ns

  def pop_ns(): Handle =
    ns match {
      case h :: t =>
        ns = t
        h
      case _ =>
        throw new Exception("[pop_ns] no more handle in n.s.")
    }

  def top_ns(): Handle =
    ns match {
      case h :: _ => h
      case _ => throw new Exception("failed to get top of namespace")
    }

  def lookup(lval: (Handle, String)): Rval = {
    val (handle, fieldname) = lval
    if (heap contains handle) {
      if (heap(handle) contains fieldname)
        heap(handle)(fieldname)
      else {
        if (heap(handle) contains parent_ns_key)
          heap(handle)(parent_ns_key) match {
            case Handle(loc) => lookup(Handle(loc), fieldname)
            case _ =>
              print_heap()
              throw new Exception("lookup error: " + handle + ":" + fieldname)
          }
        else {
          print_heap()
          throw new Exception("lookup error: " + handle + ":" + fieldname)
        }

      }
    } else {
      print_heap()
      throw new Exception("lookup error: " + handle + ":" + fieldname)
    }
  }

  def store(lval: (Handle, String), rval: Rval, is_decl: Boolean = false): Unit = {
    val (handle, fieldname) = lval
    if (heap contains handle) {
      if (is_decl || (heap(handle) contains fieldname))
        heap += (handle -> (heap(handle) + (fieldname -> rval)))
      else {
        if (heap(handle) contains parent_ns_key)
          heap(handle)(parent_ns_key) match {
            case Handle(loc) => store((Handle(loc),fieldname), rval)
            case _ =>
              throw new Exception("Illegal value for variable: " + fieldname)
          }
        else {
          throw new Exception("undefined variable: " + fieldname)
        }
      }
    } else // if the handle is not in the heap
      throw new Exception("store error: " + handle + ":" + fieldname)
  }

  def interpretPTREE(p: Ptree): Unit = p match {
    case Program(d, c) =>
      interpretDTREE(d)
      interpretCTREE(c)
  }

  def interpretCTREE(c: Ctree): Unit = c match {
    case Assign(l, e) =>
      val lval = interpretLTREE(l)
      val rval = interpretETREE(e)
      store(lval, rval)

    case Print(e) =>
      println(interpretETREE(e))

    case Seq(c1, c2) =>
      interpretCTREE(c1)
      interpretCTREE(c2)

    case Cond(e, ct, cf) =>
      interpretETREE(e) match {
        case IntValue(n) =>
          if (n == 0) interpretCTREE(cf)
          else interpretCTREE(ct)
        case _ => throw new Exception
      }

    case Loop(e, body) =>
      interpretETREE(e) match {
        case BoolValue(b) =>
          if (b) {
            interpretCTREE(body)
            interpretCTREE(c)
          }
        case _ => throw new Exception("illegal value on Loop condition")
      }
    case Proc(f, es) =>
      val h_f = interpretLTREE(f)
      lookup(h_f) match {
        case Handle(h) =>
          if (closure contains Handle(h)) {
            val (ps, body, handle) = closure(Handle(h))
            if (es.length == ps.length) {
              val new_handle = allocateNS(parent = handle)
              push_ns(new_handle)
              for (i <- 0 to es.length - 1) {
                interpretDTREE(DVar(ps(i), es(i)))
              }
              interpretCTREE(body)
              pop_ns()

            } else {
              print_heap()
              print_closure()
              throw new Exception("[length] Undefined procedure : " + f + "(" + es + ")")
            }
          } else {
            print_heap()
            print_closure()
            throw new Exception("[closure] Undefined procedure : " + f + "(" + es + ")")
          }
        case _ =>
          print_heap()
          print_closure()
          throw new Exception("[handle] Undefined procedure : " + f + "(" + es + ")")
      }
  }

  def interpretDTREE(d: Dtree): Unit = d match {
    case DVar(id, e) =>
      val handle = top_ns()
      val value = interpretETREE(e)
      store((handle, id), value, is_decl = true)

    case DProc(f, params, c) =>
      val handle = allocateNS(parent = top_ns())
      store((top_ns(), f), handle, is_decl = true)
      closure = closure + (handle -> (params, c, top_ns()))

    case DSeq(d1, d2) =>
      interpretDTREE(d1)
      interpretDTREE(d2)
  }

  def interpretETREE(e: Etree): Rval = e match {
    case Num(n) => IntValue(n.toInt)
    case Add(e1,e2) =>
      val r1 = interpretETREE(e1)
      val r2 = interpretETREE(e2)
      r1 + r2
    case Eq(e1,e2) =>
      val r1 = interpretETREE(e1)
      val r2 = interpretETREE(e2)
      BoolValue(r1 == r2)
    case Neg(e1) =>
      interpretETREE(e1) match {
        case BoolValue(b) => BoolValue(!b)
        case rv => throw new Exception("Illegal expr value of " + rv + " on Neg(e)")
      }
    case Deref(l) =>
      lookup(interpretLTREE(l))
    case NIL => NILValue
    case New(t) =>
      interpretTTREE(t)
  }

  def interpretTTREE(t: Ttree): Handle = t match {
    case TStruct(d) =>
      val handle = allocateNS(parent=top_ns())
      push_ns(handle)
      interpretDTREE(d)
      pop_ns()
      handle

    case TArray(len, e) =>
      val handle = allocateNS(parent=top_ns())
      interpretETREE(len) match {
        case IntValue(size) =>
          val init = interpretETREE(e)
          for (i <- 0 until size)
            store((handle, i.toString), init)
          handle
        case _ => throw new Exception("Illegal size value for Array")
      }

    case _ => throw new Exception("TODO: interpretTTREE._")
  }

  def interpretLTREE(left: Ltree): (Handle, String) = left match {
    case Id(x) =>
      val handle = top_ns()
      (handle, x)

    case Dot(ls, Id(x)) =>
      val lval = interpretLTREE(ls)
      val handle = lookup(lval) match {
        case Handle(loc) => Handle(loc)
        case v =>
          print(heap)
          throw new Exception("[intpL][Dot] Will never be selected." + v)
      }
      (handle, x)

    case Arr(an, idx) =>
      val lval = interpretLTREE(an)
      val handle = lookup(lval) match {
        case Handle(loc) => Handle(loc)
        case _ => throw new Exception("[intpL][Arr] Will never be selected.")
      }
      interpretETREE(idx) match {
        case IntValue(n) =>
          (handle, n.toString)
        case _ => throw new Exception("[intpL][Arr] Illegal index value for Array")
      }

    case _ => throw new Exception("TODO: interpretLTREE._")
  }

  // Controller
  def main(args: Array[String]): Unit = {
    try {
      val source = Source.fromFile("src/main/resources/source.txt").getLines().mkString
      println("input : " + source)
      val optree = parse(source)
      println("optree : " + optree)
      heap_to_greek = false
      interpretPTREE(optree)
      println("final heap : ")
      print_heap()
    } catch {
      case e: Exception => println(e)
    }
  }
}
