// Interpreter for a Baby object-oriented language
//   with Declarations,
//        Type Templates,
//        Procedures,
//        Classes, and Modules.
//
// P : Program                 E : Expression
// D : Declaration             L : NameExpression
// T : TypeTemplate            N : Numeral
// C : Command                 I : Identifier
//
// P  ::= D ; C
// E  ::= N | L | ( E1 + E2 ) | ( E1 == E2 ) | not E | new T | nil
// C  ::= L = E | print L | C1 ; C2
//      | if E { C1 } else { C2 } | while E { C } | L()
// D  ::= var I = E | D1 ; D2 | proc I(I+) { C } | class I = T
//      | module I = D end | import L
// T  ::= struct D end | array [ E1 ] of E2 | L
// L  ::= I | L . I | L [ E ]
// N  ::= string of digits
// I  ::= strings of letters, not including keywords
//
// Operator Tree
// PTREE ::= Program(DTREE, CTREE)
// ETREE ::= Num(NUM) | Deref(LTREE) | Add(ETREE, ETREE)
//         | Eq(ETREE, ETREE) | Neq(ETREE) | New(TREE) | NIL
// CTREE ::= Assign(LTREE, ETREE) | Print(LTREE) | Seq(CTREE, CTREE)
//         | Cond(ETREE, CTREE, CTREE) | Loop(ETREE, CTREE) | Proc(ID, ETREE*)
// DTREE ::= DVar(ID, ETREE) | DSeq(DTREE, DTREE) | DProc(ID, ID*, C)
//         | Class(ID, TTREE) | Module(ID, DTREE) | Import(LTREE)
// TTREE ::= TStruct(DTREE) | TArray(ETREE, ETREE) | TName(LTREE)
// LTREE ::= Id(ID) | Dot(LTREE,LTREE) | Arr(LTREE, ETREE)
// NUM   ::= a nonempty string of digits
// ID    ::= a nonempty string of letters

trait OpTree {
  sealed abstract class Rval {
    override def toString(): String = {
      this match {
        case Handle(l) => "`" + l
        case Value(n) => n.toString()
        case Nil => "nil"
      }
    }

    // r1.+(r2)
    // r1 + r2
    def +(r:Rval): Rval = {
      this match {
        case Value(n1) =>
          r match {
            case Value(n2) => Value(n1 + n2)
            case _ => throw new Exception
          }
        case _ => throw new Exception
      }
    }
  }
  case class Handle(loc: Int) extends Rval
  case class Value(n: Int) extends Rval
  case class BValue(b: Boolean) extends Rval
  case object Nil extends Rval

  sealed abstract class Ltree {
    override def toString(): String = {
      this match {
        case Id(x) => x
        case Dot(ns, x) => "" + ns + "." + x
      }
    }
  }
  case class Id(x: String) extends Ltree
  case class Dot(ns: Ltree, x:Ltree) extends Ltree

  sealed abstract class Etree {
    override def toString(): String = {
      this match {
        case Num(s) => s
        case Add(e1, e2) => "" + e1 + " + " + e2
        case Deref(l) => l.toString()
        case New(os) =>
          os match {
            case h :: t => "new { " + t.foldLeft(h)(_ + "," + _) + " }"
            case _ => "new {}"
          }
      }
    }
  }
  case class Num(s: String) extends Etree
  case class Add(e1: Etree, e2: Etree) extends Etree
  case class Deref(l: Ltree) extends Etree
  case class New(o: List[String]) extends Etree

  sealed abstract class Ctree {
    override def toString(): String = {
      this match {
        case Assign(l, e) => "" + l + " = " + e
        case Cond(e,ct,cf) =>
          "if " + e + " : " +
            (ct match {
              case h :: t => t.foldLeft(h.toString())(_ + ";" + _)
              case _ => ""
            }) +
          " else " +
            (cf match {
              case h :: t => t.foldLeft(h.toString())(_ + ";" + _)
              case _ => ""
            }) +
          " end"
        case Print(l) => "print " + l
      }
    }
  }
  case class Assign(l: Ltree, e: Etree) extends Ctree
  case class Cond(e: Etree, ct: List[Ctree], cf: List[Ctree]) extends Ctree
  case class Print(L: Ltree) extends Ctree
  case class Seq(c1: Ctree, c2: Ctree) extends Ctree
}

import scala.io.Source
import scala.util.parsing.combinator.JavaTokenParsers

object BabyOO extends JavaTokenParsers with OpTree {
  // Parser
  def parse(source: String): List[Ctree] =
    parseAll(prog, source) match {
      case Success(optree,_) => optree
      case _ => throw new Exception("Parse error!")
    }

  // Program P ::= CL
  def prog: Parser[List[Ctree]] = commlist

  // CommandList CL ::= C | C ; CL
  def commlist: Parser[List[Ctree]] = rep1sep(comm, ";")

  // Command C ::= L = E | print L | if E : CL1 else CL2 end
  def comm: Parser[Ctree] =
    left~("="~>expr) ^^ { case l~e => Assign(l,e) } |
    "print"~>left ^^ { case l => Print(l) } |
    ("if"~>expr<~":")~(commlist<~"else")~(commlist<~"end") ^^
      { case e~ct~cf => Cond(e,ct,cf) }

  // Expression E ::= N | ( E1 + E2 ) | L | new { I,* }
  def expr: Parser[Etree] =
    wholeNumber ^^ (Num(_)) |
    "("~>expr~"+"~expr<~")" ^^ {
      case e1~"+"~e2 => Add(e1,e2)
    } |
    "new"~("{"~>rep1sep(ident, ",")<~"}") ^^ {
      case _~ids => New(ids)
    } |
    left ^^ (Deref(_))

  // LefthandSide L ::= I | L . I
  def left: Parser[Ltree] =
    rep1sep(ident, ".") ^^ {
      case ids =>
        val h :: t = ids
        t.foldLeft(Id(h):Ltree)((l, id) => Dot(l, Id(id)))
    }

  // Interpreter
  var heap : Map[Handle, Map[String,Rval]] = Map()

  def allocateNS(): Handle = {
    var newhandle = Handle(heap.size)
    heap += (newhandle -> Map())
    newhandle
  }

  val ns = allocateNS()

  def lookup(lval: (Handle, String)): Rval = {
    val (handle, fieldname) = lval
    if ((heap contains handle) &&
        (heap(handle) contains fieldname))
      heap(handle)(fieldname)
    else
      throw new Exception("lookup error: " + handle)
  }

  def store(lval: (Handle, String), rval: Rval): Unit = {
    val (handle, fieldname) = lval
    if (heap contains handle) {
      heap += (handle -> (heap(handle) + (fieldname -> rval)))
    } else // if the handle is not in the heap
      throw new Exception("store error: " + handle)
  }

  def PTREEtoStr(p: List[Ctree]): String = {
    p match {
      case h :: t => t.foldLeft(h.toString())(_ + ";" + _)
      case _ => ""
    }
  }

  def interpretPTREE(p: List[Ctree]): Unit = interpretCLIST(p)

  def interpretCLIST(cs: List[Ctree]): Unit =
    for (c <- cs) yield interpretCTREE(c)

  def interpretCTREE(c: Ctree): Unit = c match {
    case Assign(l, e) => {
      val lval = interpretLTREE(l)
      val rval = interpretETREE(e)
      store(lval, rval)
    }
    case Cond(e, ct, cf) => {
      interpretETREE(e) match {
        case Value(n) =>
          if (n == 0) interpretCLIST(cf)
          else interpretCLIST(ct)
        case _ => throw new Exception
      }
    }
    case Print(l) => {
      println(lookup(interpretLTREE(l)))
    }
  }

  def interpretETREE(e: Etree): Rval = e match {
    case Num(n) => Value(n.toInt)
    case Add(e1,e2) =>
      val r1 = interpretETREE(e1)
      val r2 = interpretETREE(e2)
      r1 + r2
    case Deref(l) =>
      lookup(interpretLTREE(l))
    case New(fields) =>
      val newhandle = allocateNS()
      for (f <- fields) yield
        store((newhandle,f), Nil)
      newhandle
  }

  def interpretLTREE(left: Ltree): (Handle, String) = left match {
    case Id(x) => (ns, x)
    case Dot(ls, Id(x)) => {
      val lval = interpretLTREE(ls)
      val handle = lookup(lval) match {
        case Handle(loc) => Handle(loc)
        case _ => throw new Exception("Will never be selected.")
      }
      (handle, x)
    }
    case _ => throw new Exception("Will never be selected.")
  }

  // Controller
  def main(args: Array[String]): Unit = {
    try {
      println("2017000000")
      val source = Source.fromFile("src/main/resources/source.txt").getLines().mkString
      println("input : " + source)
      val optree = parse(source)
      println("optree : " + PTREEtoStr(optree))
      interpretPTREE(optree)
      println("final heap : " + heap)
    }
    catch { case e: Exception => println(e) }
  }
}
