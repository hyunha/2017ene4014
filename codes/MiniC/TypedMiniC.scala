// Interpreter for a Typed C-like mini-language with pointers
//
// Program P ::= CL
// CommandList CL ::= C | C ; CL
// Command C ::= L = E | print L | while E : CL end | T I
// Type T ::= int | T *
// Expression E ::= N | ( E1 + E2 ) | L | &L
// LefthandSide L ::= I | *L | I[E]
// Numeral N ::= string of digits
// Variable I ::= strings of letters, not including keywords: while, print, end
//
// Operator Tree
// PTREE ::= List[CTREE]
// CTREE ::= Assign(LTREE,ETREE) | While(ETREE,CLIST) | Print(LTREE)
// ETREE ::= Num(String) | Add(ETREE,ETREE) | Sub(ETREE,ETREE) | At(LTREE) | Amph(LTREE)
// LTREE ::= Var(String) | Star(LTREE)

trait TypedOpTree {
  sealed abstract class Ltree
  case class Var(x: String) extends Ltree
  case class Star(l: Ltree) extends Ltree
  case class Arr(x: String, i: Etree) extends Ltree

  sealed abstract class Etree
  case class Num(s: String) extends Etree
  case class Add(e1: Etree, e2: Etree) extends Etree
  case class Sub(e1: Etree, e2: Etree) extends Etree
  case class At(l: Ltree) extends Etree
  case class Amph(l: Ltree) extends Etree

  sealed abstract class Ctree
  case class Assign(l: Ltree, e: Etree) extends Ctree
  case class While(e: Etree, c: List[Ctree]) extends Ctree
  case class Print(L: Ltree) extends Ctree
  case class Decl(x: String, t:List[String]) extends Ctree
}

import scala.io.Source
import scala.util.parsing.combinator.JavaTokenParsers

object TypedMiniC extends JavaTokenParsers with TypedOpTree {
  // Parser
  def parse(source: String): List[Ctree] =
    parseAll(prog, source) match {
      case Success(optree,_) => optree
      case _ => throw new Exception("Parse error!")
    }

  // Program P ::= CL
  def prog: Parser[List[Ctree]] = commlist

  // CommandList CL ::= C | C ; CL
  def commlist: Parser[List[Ctree]] = rep1sep(comm, ";")

  // Command C ::= L = E | print L | while E : CL end
  def comm: Parser[Ctree] =
    left~("="~>expr) ^^ { case l~e => Assign(l, e) } |
    "print"~>left ^^ { case l => Print(l) } |
    ("while"~>expr<~":")~(commlist<~"end") ^^ { case e~cs => While(e, cs) } |
    typ~ident ^^ { case t~x => Decl(x, t) }

  // Expression E ::= N | ( E1 + E2 ) | L | &L
  def expr: Parser[Etree] =
    wholeNumber ^^ (Num(_)) |
      "("~>expr~op~expr<~")" ^^ {
        case e1~"+"~e2 => Add(e1,e2)
        case e1~"-"~e2 => Sub(e1,e2)
      } |
      left ^^ (At(_)) |
      "&"~>left ^^ (Amph(_))

  // LefthandSide L ::= I | *L | I[E]
  def left: Parser[Ltree] =
    ident~("["~>expr<~"]") ^^ {
      case id~i => Arr(id, i)
    } |
    ident ^^ (Var(_)) |
    "*"~>left ^^ (Star(_))

  def typ: Parser[List[String]] =
    "int"~rep("*") ^^ {
      case t~ptrs => {
        ptrs.map((_ => "ptr")) ++ List("int")
      }
    }

  def op: Parser[String] = "+" | "-"
  // Interpreter
  val memory = scala.collection.mutable.ArrayBuffer.empty[Int]
  var env = Map.empty[String,(Int, List[String])]

  def interpretPTREE(p: List[Ctree]): Unit = interpretCLIST(p)

  def interpretCLIST(cs: List[Ctree]): Unit =
    for (c <- cs) yield {
      println(c)
      interpretCTREE(c)
      print_env()
      print_memory()
    }

  def interpretCTREE(c: Ctree): Unit = {
    c match {
      case Assign(l, e) => {
        val (lval, ltyp) = interpretLTREE(l)
        val (exprval, etyp) = interpretETREE(e)
        if (ltyp == etyp)
          memory(lval) = exprval // do the assignment
        else throw new Exception("incompatible types for assignment")
      }
      case Print(l) => {
        val (loc, typ) = interpretLTREE(l)
        println(memory(loc))
      }
      case While(e, cs) => {
        val (cond, typ) = interpretETREE(e)
        if (cond != 0) {
          interpretCLIST(cs)
          interpretCTREE(c)
        }
      }
      case Decl(x,typ) => {
        if (env contains x)
          throw new Exception("vriable " + x + " redeclared")
        else {
          memory += 0 // add a cell at the end of memory
          env += (x -> (memory.length - 1, typ)) // save type and location for x
        }
      }
    }
  }

  def interpretETREE(e: Etree): (Int, List[String]) = e match {
    case Num(n) => (n.toInt, List("int"))
    case Add(e1,e2) =>
      val (n1, typ1) = interpretETREE(e1)
      val (n2, typ2) = interpretETREE(e2)
      if (typ1 == List("int") && typ2 == List("int"))
        (n1 + n2, List("int"))
      else throw new Exception("cannot do arithmetic on non-ints")
    case Sub(e1,e2) =>
      val (n1, typ1) = interpretETREE(e1)
      val (n2, typ2) = interpretETREE(e2)
      if (typ1 == List("int") && typ2 == List("int"))
        (n1 - n2, List("int"))
      else throw new Exception("cannot do arithmetic on non-ints")
    case At(l) =>
      val (n, typ0) = interpretLTREE(l)
      (memory(n), typ0)
    case Amph(l) =>
      val (n, typ0) = interpretLTREE(l)
      (n, "ptr" :: typ0)
  }

  def interpretLTREE(l: Ltree): (Int, List[String]) = l match {
    case Var(x) => {
      if (env contains x)
        env(x)
      else throw new Exception(x + " is not declared!")
    }
    case Star(l) => { // a pointer dereference
      val (loc, typ) = interpretLTREE(l) // get a location number
      typ match {
        case (h::t) =>
          if (h == "ptr") (memory(loc), t)
          else throw new Exception("variable not a pointer")
        case Nil => throw new Exception("no such a case occurs")
      }
    }

    case Arr(x, i) => {
      env(x)
    }
  }

  def print_memory(): Unit = {
    println("[memory]")
    if (memory.isEmpty) {
      println("  empty!")
    } else {
      var length = memory.size.toString.length
      var vlength = memory.max.toString.length
      var width = if (vlength > length) vlength else length
      var i = 0
      println("-" * (memory.size * (width + 3) + 7))
      print("|")
      for (i <- 0 to memory.size - 1) {
        print(" " * (width - i.toString.length + 1) + i + " |")
      }
      println(" ... |")
      println("-" * (memory.size * (width + 3) + 7))
      print("|")
      for (k <- memory) {
        print(" " * (width - k.toString.length + 1) + k + " |")
      }
      println(" ... |")
      println("-" * (memory.size * (width + 3) + 7))
    }
  }

  def typ_to_string(typ:List[String]): String =
    typ match {
      case Nil => ""
      case "ptr" :: t => typ_to_string(t) + "*"
      case h :: t => h + typ_to_string(t)
    }

  def print_env(): Unit = {
    println("[namespace]")
    if (env.isEmpty) {
      println("  empty!")
    } else {
      var k_max = 0
      var v_max = 0
      var t_max = 0
      for ((k, vt) <- env) yield {
        k_max = if (k.length > k_max) k.length else k_max
        v_max = if (vt._1.toString.length > v_max) vt._1.toString.length else v_max
        val t_str = typ_to_string(vt._2)
        t_max = if (t_str.length > t_max) t_str.length else t_max
      }
      var i = 0
      println("-" * (k_max + v_max + t_max + 11))
      for ((k, vt) <- env) yield {
        print("| " + " " * (k_max - k.length) + k + " -> ")
        print(" " * (v_max - vt._1.toString.length) + vt._1 + " : ")
        val t_str = typ_to_string(vt._2)
        println(t_str + " " * (t_max - t_str.length) + " |")
      }
      println("-" * (k_max + v_max + t_max + 11))
    }
  }

  // Controller
  def main(args: Array[String]): Unit = {
    try {
      val source = Source.fromFile("src/main/resources/typed_source.txt").getLines().mkString
      println("input : " + source)
      val optree = parse(source)
      println("optree : " + optree)
      interpretPTREE(optree)
      println("final memory : " + memory)
      print_memory()
      println("final namespace : " + env)
      print_env()
    }
    catch { case e: Exception => println(e) }
  }
}
